bleachbit (4.6.2-1) unstable; urgency=medium

  * New upstream version 4.6.2
  * d/salsa-ci.yml: add licenserecon
  * add d/lrc.config for exclude possible false-positive with licenserecon
  * d/patches: remove 2 patches applied upstream and refresh other
  * Update d/copyright

 -- Fabio Fantoni <fantonifabio@tiscali.it>  Sat, 09 Nov 2024 10:44:05 +0100

bleachbit (4.6.0-4) unstable; urgency=medium

  * Update standards version to 4.7.0, no changes needed
  * Add missed entries on d/copyright (Closes: #1077071)
  * Add 2 upstream fixes for invalid escape sequence SintaxWarning
    from python 3.12 (Closes: #1079366)
  * d/rules:
    - remove executable from app-menu.ui
    - remove Windows-specific modules

 -- Fabio Fantoni <fantonifabio@tiscali.it>  Thu, 29 Aug 2024 11:07:00 +0000

bleachbit (4.6.0-3) unstable; urgency=medium

  * Return to Debian Python Team and add myself to uploaders (Closes: #1065808)
  * Remove libgtk-3-0 from depends (Closes: #1067171)

 -- Fabio Fantoni <fantonifabio@tiscali.it>  Tue, 19 Mar 2024 21:57:51 +0100

bleachbit (4.6.0-2) unstable; urgency=medium

  * QA upload.
  * Orphan the package.

  [ Fabio Fantoni ]
  * Add upstream signing key and check for signature in d/watch.
  * Add debian/salsa-ci.yml.
  * Update d/copyright.

 -- Boyuan Yang <byang@debian.org>  Sat, 09 Mar 2024 20:48:07 -0500

bleachbit (4.6.0-1) unstable; urgency=medium

  * New upstream release.
    + Fix compatibility with python3.11. (Closes: #1058148)
  * Update uploaders list. (Closes: #1050264)
  * debian/: Apply "wrap-and-sort -abst".

 -- Boyuan Yang <byang@debian.org>  Fri, 15 Dec 2023 23:11:35 -0500

bleachbit (4.4.2-2) unstable; urgency=medium

  * Team upload
  * d/control: remove policykit-1 depends (Closes: #1025545)

 -- Fabio Fantoni <fantonifabio@tiscali.it>  Thu, 10 Aug 2023 11:21:13 +0000

bleachbit (4.4.2-1) unstable; urgency=medium

  * Team upload

  [ Fabio Fantoni ]
  * New upstream release (Closes: #958344, #970229, #974530, #994094)
    (LP: #1957053, #1875809, #1933289)
  * Bump debhelper-compat to 13
  * d/control: add missed dependency gir1.2-notify-0.7
  * update d/copyright
  * d/control: specify that rules don't require root

  [ Jeremy Bicha ]
  * debian/control: Depend on pkexec

 -- Fabio Fantoni <fantonifabio@tiscali.it>  Thu, 17 Mar 2022 08:55:46 -0400

bleachbit (3.9.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Wed, 08 Sep 2021 20:35:55 -0400

bleachbit (3.9.0-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright
    - update upstream copyright notice
  * Switch to python3; Closes: #936214

 -- Sandro Tosi <morph@debian.org>  Wed, 01 Apr 2020 00:08:31 -0400

bleachbit (3.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Hugo Lefeuvre ]
  * New upstream release.
  * Run wrap-and-sort -a.
  * Refresh manpage for new upstream release.
  * Refresh patches for new upstream release.
  * Remove debian/lintian-overrides: warning not present anymore.
  * debian/copyright: Add missing CC0-1.0 entry for bleachbit.appdata.xml.
  * debian/control:
    - Remove pygtk dependency, upstream moved to GTK 3 as part of the 3.0
      release (Closes: #885261).
    - Add missing python-chardet, python-gi, python-requests and python-scandir
      dependencies.
    - Bump Standards-Version to 4.4.1.
    - Move python-setuptools to Build-Depends-Indep.
  * debian/install: Add rule to install data/app-menu.ui.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 27 Oct 2019 10:33:08 +0100

bleachbit (2.2-3) unstable; urgency=medium

  * Fix autopkgtests, broken since 2.2-1 (Closes: #932730).

 -- Hugo Lefeuvre <hle@debian.org>  Mon, 22 Jul 2019 11:21:14 -0300

bleachbit (2.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Hugo Lefeuvre ]
  * Upload to unstable.
  * debian/control:
    - Bump Standards-Version to 4.4.0.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 21 Jul 2019 19:48:34 -0300

bleachbit (2.2-1) experimental; urgency=medium

  * New upstream release (Closes: #922368) (Closes: #928909).
    - remove fix-deep-scan-unidecode-error.patch (applied upstream).
  * debian/control:
    - Bump Standards-Version to 4.3.0.3.
    - Update debhelper dependency from >= 11 to >= 12.
    - Add newly required python-setuptools dependency.
  * remove firefox_profiles.patch (no longer needed).
  * debian/rules:
    - Fix egg-info removal.
    - Fix removal of windows-specific cleaners.
    - Migrate to Pybuild.
  * update manpage for new upstream release.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 09 Jun 2019 20:12:36 +0200

bleachbit (2.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/watch: Use https protocol

  [ Hugo Lefeuvre ]
  * debian/control:
    - Bump Standards-Version to 4.3.0.
  * debian/copyright:
    - Update copyright years.
  * debian/patches/fix-deep-scan-unidecode-error.patch:
    - fix UnicodeDecodeError while running deepscan (Closes: #895979).

 -- Hugo Lefeuvre <hle@debian.org>  Wed, 30 Jan 2019 19:57:28 +0100

bleachbit (2.0-2) unstable; urgency=medium

  * Use policykit instead of su-to-root (Closes: #855462).
    - Add required policykit-1 dependency to debian/control.
    - No longer depend on menu (was used for su-to-root, no longer relevant).
    - Update root desktop file in debian/rules.
    - Install policykit rule.
  * Run wrap-and-sort -a.

 -- Hugo Lefeuvre <hle@debian.org>  Fri, 23 Mar 2018 09:25:35 +0100

bleachbit (2.0-1) unstable; urgency=medium

  * New upstream release.
  * Remove trailing whitespaces in various files (help from Ondrej, thanks!).
  * debian/control:
    - Bump Standards-Version to 4.1.3.
    - Update debhelper dependency from >= 10 to >= 11.
    - Add missing desktop-file-utils dependency.
  * debian/copyright:
    - Update copyright years.
  * Refresh patches for new upstream release.
  * Remove local_cleaners_dir.patch, not useful anymore.
  * Update debian/watch to version 4 and remove upstream keys: not providing
    signed releases in a compatible format anymore.

 -- Hugo Lefeuvre <hle@debian.org>  Thu, 15 Mar 2018 18:42:45 +0100

bleachbit (1.15-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/control:
    - Bump Standards-Version to 4.0.0.
    - Update debhelper dependency from >= 9 to >= 10.
    - Update X-Python-Version field to >= 2.6.
  * Bump compat level to 10.
  * Use https protocol in debian/copyright links.

 -- Hugo Lefeuvre <hle@debian.org>  Wed, 30 Aug 2017 10:08:24 +0200

bleachbit (1.15-1) experimental; urgency=low

  * New upstream release.
  * debian/patches:
    - Remove debian/patches/desktop_file.patch (integrated by upstream).
    - Refresh patches for new upstream release.
  * debian/copyright:
    - Update copyright years.
  * debian/bleachbit.1:
    - Refresh manpage for new upstream release.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 02 Apr 2017 13:19:20 +0200

bleachbit (1.12-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release.

 -- Hugo Lefeuvre <hle@debian.org>  Thu, 28 Jul 2016 23:04:58 +0200

bleachbit (1.11.2-1) experimental; urgency=low

  * New upstream release.
  * Remove debian/patches/gnomevfs.patch (integrated by upstream).
  * debian/control:
    - Update Homepage.
    - Bump Standards-Version to 3.9.8 (no changes needed).
    - Fix unnecessary versioned dependency python in the Build-Depends-Indep
      field.
    - Fix unnecessary versioned dependencies python and python-gtk2 in the
      Depends field.
  * debian/copyright:
    - Update Format field to match DEP5 recommendations.
  * debian/patches/local_cleaners_dir.patch:
    - Use HTTPS protocol in the BTS link.

 -- Hugo Lefeuvre <hle@debian.org>  Sat, 18 Jun 2016 13:22:14 +0200

bleachbit (1.10-1) unstable; urgency=low

  * Upload to unstable.
  * New upstream release.
  * Remove debian/menu to make the package CTTE #741573 compliant.
  * debian/control:
    - Bump Standards-Version to 3.9.7 (no changes needed).
  * debian/patches/*.patch:
    - Refresh for new upstream release.
  * debian/copyright:
    - Update copyright years.
  * debian/bleachbit.1:
    - Refresh manpage for new upstream release.

 -- Hugo Lefeuvre <hle@debian.org>  Thu, 24 Mar 2016 11:21:20 +0100

bleachbit (1.9.1-1) experimental; urgency=low

  * New upstream release.
  * debian/control:
    - Use https protocol for Vcs-Browser.
    - Add dh-python to Build-Depends.
    - Remove useless 'XS-Testsuite: autopkgtest' field.
  * debian/copyright, debian/control:
    - Change the Maintainer's e-mail address.
  * debian/patches:
    - Refresh patches for new usptream release.
    - Make patches DEP3 compliant.
  * debian/watch, debian/upstream:
    - Verify upstream releases using its public key.
  * debian/bleachbit.1:
    - Refresh manpage for new upstream release.

 -- Hugo Lefeuvre <hle@debian.org>  Tue, 22 Sep 2015 12:33:38 +0200

bleachbit (1.8-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright:
    - Update copyright years.
  * debian/patches/*.patch:
    - Refresh for new upstream release.

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Thu, 25 Jun 2015 19:47:27 +0200

bleachbit (1.6-1) unstable; urgency=low

  * Upload to unstable.
  * New upstream release.
  * debian/patches/*.patch:
    - Refresh for new upstream release.

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Sat, 22 Nov 2014 11:51:23 +0100

bleachbit (1.5.2~b1-1) experimental; urgency=low

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 3.9.6 (no changes needed).
  * debian/patches/*.patch:
    - Refresh for new upstream release.
  * debian/patches/desktop_file.patch:
    - Add a Keyword entry to make bleachbit fully Freedesktop compliant.

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Sat, 08 Nov 2014 22:17:41 +0100

bleachbit (1.4-1) unstable; urgency=low

  * New upstream release.
  * debian/control, debian/copyright:
    - New Maintainer (Closes: #755293).
  * debian/patches/series, debian/patches/firefox_profiles.patch:
    - Added firefox_profiles.patch (Closes: #752257).

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Sat, 13 Sep 2014 15:32:42 +0200

bleachbit (1.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/*.patch:
    - Refresh for new upstream release.
  * debian/copyright:
    - Update copyright years.
  * debian/tests/*:
    - Provide a simple test to check whether the package is functional
      as per DEP8 (autopkgtests).

 -- Luca Falavigna <dktrkranz@debian.org>  Wed, 11 Jun 2014 23:09:35 +0200

bleachbit (1.0-1) unstable; urgency=low

  * New upstream release (Closes: #725789).
  * debian/patches/desktop_file.patch:
    - Refresh for new upstream release.
  * debian/bleachbit.1:
    - Refresh man page with the newly added options.
  * debian/control:
    - Bump Standards-Version to 3.9.5.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 24 Nov 2013 17:41:12 +0100

bleachbit (0.9.6-1) unstable; urgency=low

  * New upstream release.

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 20 Jul 2013 10:57:57 +0200

bleachbit (0.9.5-2) unstable; urgency=low

  * Upload to unstable.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 05 May 2013 14:34:45 +0200

bleachbit (0.9.5-1) experimental; urgency=low

  * New upstream release (Closes: #699786).
  * debian/patches/desktop_file.patch
    debian/patches/gnomevfs.patch,
    debian/patches/local_cleaners_dir.patch,
    - Refresh for new upstream release.
  * debian/control:
    - Use canonical URIs for VCS fields.
    - Bump Standards-Version to 3.9.4.
  * debian/copyright:
    - Update copyright years.

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 09 Feb 2013 11:47:49 +0100

bleachbit (0.9.3-1) experimental; urgency=low

  * New upstream release.
  * debian/patches/no_X.patch:
    - Removed, applied upstream.
  * debian/bleachbit.xpm:
    - Convert to the new icon.

 -- Luca Falavigna <dktrkranz@debian.org>  Mon, 20 Aug 2012 23:05:43 +0200

bleachbit (0.9.2-2) unstable; urgency=low

  * debian/patches/no_X.patch:
    - Do not try to launch GUI if X is not available (Closes: #674485).
  * debian/compat:
    - Bump compatibility level to 9.
  * debian/rules:
    - Delete .mo files during clean phase (Closes: #671414).

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 26 May 2012 18:28:15 +0200

bleachbit (0.9.2-1) unstable; urgency=low

  * New upstream release.
   * debian/control:
     - Bump Standards-Version to 3.9.3.
  * debian/copyright:
    - Adjust copyright years.
    - Format now points to copyright-format site.

 -- Luca Falavigna <dktrkranz@debian.org>  Fri, 16 Mar 2012 21:43:26 +0100

bleachbit (0.9.1-1) unstable; urgency=low

  * New upstream release.

 -- Luca Falavigna <dktrkranz@debian.org>  Fri, 11 Nov 2011 20:20:41 +0100

bleachbit (0.9.0-1) unstable; urgency=low

  * New upstream release.
    - Use word 'Clean' instead of 'Delete' (Closes: #626883).
  * debian/patches/desktop_file.patch, debian/patches/gnomevfs.patch:
    - Refresh for new upstream release.

 -- Luca Falavigna <dktrkranz@debian.org>  Mon, 29 Aug 2011 22:29:46 +0200

bleachbit (0.8.8-2) unstable; urgency=low

  * Install new loader (LP: #797328).
    + debian/bleachbit.1:
      - Reworked to include CLI parameters.
    + debian/bleachbit_cli.1:
      - Removed, no longer needed.
    + debian/install:
      - Install bleachbit.py file.
    + debian/links:
      - Adjust bleachbit link to the new loader.
      - No longer provide link to CLI interface.
    + debian/manpages:
      - No longer install bleachbit_cli.1, removed.
    + debian/rules:
      - Adjust permissions of bleachbit.py.
  * debian/copyright:
    - Convert to DEP5 format.
  * debian/rules:
    - Remove egg-info information from binary package.

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 02 Jul 2011 10:56:33 +0200

bleachbit (0.8.8-1) unstable; urgency=low

  * New upstream release.
    - Fix crash when changing options (LP: #715286).
  * debian/patches/gnomevfs.patch:
    - Refresh for new upstream release.
  * debian/control:
    - Depend on menu (Closes: #630899) (LP: #632989).
    - Bump Standards-Version to 3.9.2, no changes required.

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 18 Jun 2011 18:14:41 +0200

bleachbit (0.8.7-1) unstable; urgency=low

  * New upstream release (LP: #713537).
  * Refresh patches for new upstream release.
  * Switch to dh_python2.
  * debian/patches/gnomevfs.patch:
    - Replace gnomevfs methods, now deprecated.
  * debian/bleachbit_cli.1:
    - Document latest option implemented upstream.
  * debian/control:
    - Depends on python (>= 2.6) | python-simplejson.
    - Drop python-glade2 dependency.
    - Drop python-gnome2 dependency (Closes: #605555).
    - No longer recommend menu.
    - Bump Standards-Version to 3.9.1, no changes required.
  * debian/copyright:
    - Update copyright years.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 06 Feb 2011 18:39:49 +0100

bleachbit (0.8.0-1) unstable; urgency=low

  * New upstream release.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 06 Jun 2010 11:43:36 +0200

bleachbit (0.7.4-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstream release.
  * debian/install:
    - Also provide bleachbit.png file.

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 15 Apr 2010 22:52:40 +0200

bleachbit (0.7.3-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 3.8.4, no changes required.
  * debian/copyright:
    - Update copyright years.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 21 Feb 2010 15:10:00 +0100

bleachbit (0.7.2-2) unstable; urgency=low

  * debian/patches/local_cleaners_dir.patch:
    - Point local_cleaners_dir to personal_cleaners_dir. The former is
      not useful when BleachBit is installed system-wide, this way users
      are not bothered for new cleaners each time (Closes: #565359).
  * Switch to format 3.0 (quilt).

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 24 Jan 2010 21:28:06 +0100

bleachbit (0.7.2-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/GUI_relative_imports.patch:
    - Removed, applied upstream.
  * debian/install, debian/rules:
    - No longer generate bleachbit-root.desktop in build directory, this
      way build is automatically removed during clean. Generate .desktop
      file into the correct location directly.

 -- Luca Falavigna <dktrkranz@debian.org>  Wed, 02 Dec 2009 22:12:24 +0100

bleachbit (0.7.1-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstream release.
  * Use debhelper buildsystem feature.

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 10 Nov 2009 22:48:59 +0100

bleachbit (0.7.0-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/GUI_relative_imports.patch:
    - Refreshed for new upstream release.

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 22 Oct 2009 21:49:11 +0200

bleachbit (0.6.5-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/GUI_relative_imports.patch:
    - Do not use absolute imports, we install bleachbit module in a
      private directory.

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 01 Oct 2009 22:02:24 +0200

bleachbit (0.6.4-1) unstable; urgency=low

  * New upstream release:
    - New feature: command line inferface (CLI).
  * Add manpage for bleachbit_cli script.

 -- Luca Falavigna <dktrkranz@debian.org>  Wed, 16 Sep 2009 23:39:09 +0200

bleachbit (0.6.3-1) unstable; urgency=low

  * New upstream release.

 -- Luca Falavigna <dktrkranz@debian.org>  Fri, 28 Aug 2009 08:08:08 +0000

bleachbit (0.6.2-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/no_update:
    - Refresh patch for new upstream release.
  * debian/control:
    - Update my e-mail address.
    - Adjust Homepage field to match new location.
    - Bump Standards-Version to 3.8.3, no changes required.
  * debian/watch:
    - Upstream provides .tar.gz archives, get those instead of .tar.bz2.

 -- Luca Falavigna <dktrkranz@debian.org>  Wed, 26 Aug 2009 21:59:06 +0200

bleachbit (0.5.4-1) unstable; urgency=low

  * New upstream release.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Sun, 19 Jul 2009 22:16:44 +0200

bleachbit (0.5.3-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstream release:
    - debian/patches/desktop_file
  * Adjust Homepage field and download location to match current ones.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Sun, 05 Jul 2009 21:45:32 +0200

bleachbit (0.5.2-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstream release:
    - debian/patches/desktop_file
    - debian/patches/no_update
  * Do not install Windows-specific cleaners, (as seen in release notes).
  * Bump Standards-Version to 3.8.2, no changes required.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Wed, 24 Jun 2009 23:39:54 +0200

bleachbit (0.5.1-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstream release:
    - debian/patches/desktop_file
    - debian/patches/no_update
  * debian/patches/suppress_locales_warnings:
    - Removed, fixed upstream.
  * Bump debhelper minimum version to 7.0.50 for override_* support.
  * Install only *.xml files from cleaners directory.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Sat, 13 Jun 2009 19:45:53 +0200

bleachbit (0.4.2-1) unstable; urgency=low

  * New upstream release.
  * Switch to debhelper 7.
  * debian/patches/launcher:
    - Remove it, no longer needed because launcher has been replaced by
      a symlink to usr/share/bleachbit/bleachbit/GUI.py file.
  * debian/rules:
    - Make /usr/share/bleachbit/bleachbit/GUI.py executable.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Tue, 12 May 2009 20:35:05 +0200

bleachbit (0.4.1-1) unstable; urgency=low

  * New upstream release.
    - French translation improved (Closes: #518951).
  * Remove patches merged upstream:
    - debian/patches/LANG_env
    - debian/patches/no_X
  * Refresh patches for new upstream release:
    - debian/patches/launcher
  * Remove python-dev from Build-Depends-Indep, use python instead.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Sun, 19 Apr 2009 14:50:20 +0200

bleachbit (0.4.0-2) unstable; urgency=low

  * debian/patches/LANG_env:
    - Fix AttributeError if LANG is not set (Closes: #519027).
  * debian/patches/no_X:
    - Do not show intrusive pyGTK warnings if X is not available, only
      display a message X is required to run BleachBit (Closes: #520107).
  * debian/patches/suppress_locales_warnings:
    - Suppress locale files warning messages when they are actually not
      locale files (Closes: #521240).
  * Bump Standards-Version to 3.8.1, no changes required.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Mon, 06 Apr 2009 14:29:23 +0200

bleachbit (0.4.0-1) unstable; urgency=low

  * New upstream release.
    - New feature: CleanerML files.
  * Refresh patches for new upstream release:
    - debian/patches/desktop_file
    - debian/patches/launcher
  * Provide a bleachbit-root.desktop file to enable superuser tasks:
    - debian/rules: create the new file on top of bleachbit.desktop.
    - debian/bleachbit.install: install new .desktop file.
    - debian/control: let bleachbit recommends menu package.
  * Add locales support:
    - debian/rules: add command to create and install .mo files.
    - debian/control: add gettext build-dependency.
  * Install example files: doc/*.xml and doc/*.xsd.
  * Switch to python-support.
  * debian/rules: remove po/*.mo files, fix FTBFS if built twice in a row.
  * debian/copyright: adjust copyright years.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Sat, 07 Mar 2009 20:54:59 +0100

bleachbit (0.2.1-1) unstable; urgency=low

  * Initial release (Closes: #510480).

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Sat, 10 Jan 2009 12:03:37 +0100
