Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bleachbit
Upstream-Contact: Andrew Ziem <ahz001@gmail.com>
Source: https://sourceforge.net/projects/bleachbit/files/

Files: *
Copyright: 2008-2024 Andrew Ziem <ahz001@gmail.com>
           2015, Jeremy Singer-Vine
           2009-2021, Rosetta Contributors and Canonical Ltd
License: GPL-3+

Files: org.bleachbit.BleachBit.metainfo.xml
Copyright: 2016-2021, Andrew Ziem <ahz001@gmail.com>
License: CC0-1.0

Files: bleachbit/markovify/chain.py
 bleachbit/markovify/splitters.py
 bleachbit/markovify/text.py
 bleachbit/markovify/utils.py
Copyright: 2015, Jeremy Singer-Vine
License: Expat
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  .
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

Files: bleachbit/_platform.py
Copyright: 1999-2000, Marc-Andre Lemburg <mal@lemburg.com>
           2000-2010, eGenix.com Software GmbH <info@egenix.com>
License: Lemburg-eGenix
  Permission to use, copy, modify, and distribute this software and its
  documentation for any purpose and without fee or royalty is hereby granted,
  provided that the above copyright notice appear in all copies and that
  both that copyright notice and this permission notice appear in
  supporting documentation or portions thereof, including modifications,
  that you make.
  .
  EGENIX.COM SOFTWARE GMBH DISCLAIMS ALL WARRANTIES WITH REGARD TO
  THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
  FITNESS, IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
  INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
  FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
  NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
  WITH THE USE OR PERFORMANCE OF THIS SOFTWARE !

Files: debian/*
Copyright: 2009-2014, Luca Falavigna <dktrkranz@debian.org>
           2014-2019, Hugo Lefeuvre <hle@debian.org>
License: GPL-3+

License: GPL-3+
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  .
  This package is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  .
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
  .
  On Debian GNU/Linux systems, the complete text of the GNU General
  Public License version 3 can be found in /usr/share/common-licenses/GPL-3

License: CC0-1.0
 On Debian systems the full text of the CC0-1.0 license can be found in
 /usr/share/common-licenses/CC0-1.0
